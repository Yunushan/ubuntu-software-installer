#!/bin/bash

# 7-Nmap

if [ "$nmap_version" = "1" ];then # 1-) Nmap (Official Package)
    sudo apt -y install nmap
elif [ "$nmap_version" = "2" ];then # 2-) Nmap Latest (From .rpm file to .deb file)
    sudo apt -y install alien lynx
    nmap_latest_rpm=$(lynx -dump https://nmap.org/download.html | awk '{print $2}' | grep -i x86_64.rpm \
    | grep -iv "ncat\|nping" | head -n 1)
    wget -O /root/Downloads/nmap-latest.rpm "$nmap_latest_rpm"
    sudo alien -i --veryverbose /root/Downloads/nmap-latest.rpm
elif [ "$nmap_version" = "3" ];then # 3-) Nmap Latest (Compile From Source Code)
    sudo apt -y install alien lynx
    nmap_latest_tgz=$(lynx -dump https://nmap.org/download.html | awk '{print $2}' | grep -i .tgz | head -n 1)
    wget -O /root/Downloads/nmap-latest.tgz "$nmap_latest_tgz"
    sudo mkdir -pv /root/Downloads/nmap-latest
    tar -xvf /root/Downloads/nmap-latest.tgz -C /root/Downloads/nmap-latest --strip-components 1
    cd /root/Downloads/nmap-latest || exit 2
    ./configure
    make -j "${core:=}" && make -j "$core" install
elif [ "$nmap_version" = "4" ];then # 4-) Nmap Latest (Via Snap)
    snap install nmap
else
    echo "Out of options please choose between 1-4"
    sleep 2
fi