#!/bin/bash

# 3-Grub-Customizer

if [ "$grub_customizer_version" = "1" ];then # 1-) Grub Customizer (From PPA)
    sudo add-apt-repository -y ppa:danielrichter2007/grub-customizer
    sudo apt-get -y install grub-customizer
elif [ "$grub_customizer_version" = "2" ];then # 2-) Grub Customizer (Mainline Compile From Source)
    sudo apt -y install cmake gcc g++ libgtkmm-3.0-dev gettext libssl-dev libarchive-dev hwinfo lynx
    grub_customizer_link=$(lynx -dump https://launchpad.net/grub-customizer/ | awk '{print $2}' \
    | grep -i tar.gz | grep -i https | head -n 1)
    wget -O /root/Downloads/grub-latest.tar.gz  "$grub_customizer_link"
    sudo mkdir -pv /root/Downloads/grub-latest
    tar -xzvf /root/Downloads/grub-latest.tar.gz -C /root/Downloads/grub-latest --strip-components 1
    cd /root/Downloads/grub-latest || exit 2
    cmake . && make -j "${core:=}"
    sudo make -j "$core" install
else
    echo "Out of options please choose between 1-2"
    sleep 2
fi