#!/bin/bash

# 13- DVBlast installer

if [ "$dvblast" = "1" ];then
    sudo apt install dvblast
elif [ "$dvblast" = "2" ];then
    sudo apt install -y make
    sudo apt -y install libtool build-essential git libev-dev libssl-dev libpcre3-dev pkg-config make cmake libpcap-dev \
    libdvbpsi-dev libavcodec-dev libavformat-dev libavutil-dev libpthread-stubs0-dev libbitstream-dev
    dvblast_link=$(lynx -dump https://github.com/gfto/dvblast/tags | awk '/http/{print $2}' | grep -i ".tar.gz" | head -n 1)
    dvblast_latest_version=$(lynx -dump https://github.com/gfto/dvblast/tags | awk '/http/{print $2}' | grep -i ".tar.gz" \
    | head -n 1 | awk -F'tags/' '{print $2}' | awk -F'.tar.gz' '{print $1}')
    wget -O /root/Downloads/"$dvblast_latest_version".tar.gz "$dvblast_link"
    sudo mkdir -pv /root/Downloads/dvblast"$dvblast_latest_version"
    tar -xvf /root/Downloads/"$dvblast_latest_version".tar.gz -C /root/Downloads/dvblast"$dvblast_latest_version" --strip-components 1
    cd /root/Downloads/dvblast"$dvblast_latest_version" || exit
    make -j "$core"
    make -j "$core" install
else
    echo "Out of options please choose between 1-2"
    sleep 2
fi