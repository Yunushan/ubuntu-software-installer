#!/bin/bash

# 12-Docker

if [ "$docker_version" = "1" ];then
    sudo apt-get -y remove docker docker-engine docker.io containerd runc
    sudo apt-get update
    sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
elif [ "$docker_version" = "2" ];then
    if [ "${cpuarch:=}" = "x86_64" ];then
        containerd_stable_deb=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/ \
        | awk '/http/{print $2}' | grep -i containerd.io | tail -n 1)
        wget -O /root/Downloads/latest-containerd.deb "$containerd_stable_deb"
        sudo dpkg -i /root/Downloads/latest-containerd.deb
        docker_ce_cli=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/ \
        | awk '/http/{print $2}' | grep -iv "docker-ce-rootless" | grep -i "docker-ce-cli" | tail -n 1)
        wget -O /root/Downloads/latest-docker-ce-cli.deb "$docker_ce_cli"
        sudo dpkg -i /root/Downloads/latest-docker-ce-cli.deb
        latest_docker_deb=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/ | awk '/http/{print $2}' \
        | grep -iv "docker-ce-cli\|docker-ce-rootless" | grep -i docker-ce | head -n 1)
        wget -O /root/Downloads/latest-docker-ce.deb "$latest_docker_deb"
        sudo dpkg -i /root/Downloads/latest-docker-ce.deb
    else
        echo "Your cpu is different than x86_64"
    fi
else
    echo "Out of options please choose between 1-2"
    sleep 2
fi