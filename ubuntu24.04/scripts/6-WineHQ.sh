#!/bin/bash

# 6-WineHQ

if [ "$winehq_version" = "1" ];then # 1-) WineHQ Stable (From Official Package)
    sudo apt -y install winehq-stable
elif [ "$winehq_version" = "2" ];then # 2-) WineHQ Development (From Official Package)
    sudo apt -y install winehq-development
elif [ "$winehq_version" = "3" ];then # 3-) WineHQ Stable (With Wine Official Repository)
    if [ "${cpuarch:=}" = "x86_64" ];then
        sudo dpkg --add-architecture i386
        sudo mkdir -pm755 /etc/apt/keyrings
        sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-stable
    else
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-stable
    fi
elif [ "$winehq_version" = "4" ];then # 4-) WineHQ Development (With Wine Official Repository)
    if [ "$cpuarch" = "x86_64" ];then
        sudo dpkg --add-architecture i386
        sudo mkdir -pm755 /etc/apt/keyrings
        sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-devel
    else
        sudo dpkg --add-architecture i386
        sudo mkdir -pm755 /etc/apt/keyrings
        sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-devel
    fi
elif [ "$winehq_version" = "5" ];then # 5-) WineHQ Staging (With Wine Official Repository)
    if [ "$cpuarch" = "x86_64" ];then
        sudo dpkg --add-architecture i386
        sudo mkdir -pm755 /etc/apt/keyrings
        sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-staging
    else
        sudo mkdir -pm755 /etc/apt/keyrings
        sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-staging
    fi
else
    echo "Out of options please choose between 1-5"
    sleep 2
fi