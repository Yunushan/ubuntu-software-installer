#!/bin/bash

# Global Variables
cpuarch=$(uname -m)
superuser=$(getent group sudo | cut -d: -f4)
codename=$(lsb_release -cs | tail -n 1)
scripts_path=$(find / -name scripts | grep -i "ubuntu24.04/scripts" | grep -iv "find\|proc" | head -n 1)
core=$(nproc)
# Select Which Softwares to be Installed

choice () {
    local choice=$1
    if [[ ${opts[choice]} ]];then #toggle
        opts[choice]=
    else
        opts[choice]=+
    fi
}
PS3='Please enter your choice(s): '
while :
do
    clear
    options=("PHP ${opts[1]}" "Nginx ${opts[2]}" "Grub Customizer ${opts[3]}" "Apache ${opts[4]}" "FFmpeg ${opts[5]}" 
    "WineHQ ${opts[6]}" "Nmap ${opts[7]}" "OpenSSH ${opts[8]}" "OpenSSL ${opts[9]}" "Redis ${opts[10]}" "Jenkins ${opts[11]}"
    "Docker ${opts[12]}" "Done ${opts[13]}")
    select opt in "${options[@]}";do
        case $opt in
            "PHP ${opts[1]}")
                choice 1
                break
                ;;
            "Nginx ${opts[2]}")
                choice 2
                break
                ;;
            "Grub Customizer ${opts[3]}")
                choice 3
                break
                ;;
            "Apache ${opts[4]}")
                choice 4
                break
                ;;
            "FFmpeg ${opts[5]}")
                choice 5
                break
                ;;
            "WineHQ ${opts[6]}")
                choice 6
                break
                ;;
            "Nmap ${opts[7]}")
                choice 7
                break
                ;;
            "OpenSSH ${opts[8]}")
                choice 8
                break
                ;;
            "OpenSSL ${opts[9]}")
                choice 9
                break
                ;;
            "Redis ${opts[10]}")
                choice 10
                break
                ;;
            "Jenkins ${opts[11]}")
                choice 11
                break
                ;;
            "Docker ${opts[12]}")
                choice 12
                break
                ;;
            "Done ${opts[13]}")
                break 2
                ;;
            *) printf '%s\n' 'Please Choose Between 1-13';;
        esac
    done
done

#Options Chosen For Block
printf '%s\n\n' 'Chosen Options: '
for opt in "${!opts[@]}";do
    if [[ ${opts[opt]} ]];then
        if [ "$opt" == 1 ];then
            printf "\nPlease Choose Your Desired PHP Version \n\n1-) PHP 8.3 (Ubuntu Official Package)\n\
2-) PHP 8.4 (Ondrej PPA)\n3-) PHP 8.3 (Ondrej PPA)\n4-) PHP 8.2 (Ondrej PPA)\n5-) PHP 8.1 (Ondrej PPA)\n6-) PHP 8.0 (Ondrej PPA)\n\
7-) PHP 7.4 (Ondrej PPA)\n8-) PHP 7.3 (Ondrej PPA)\n9-) PHP 7.2 (Ondrej PPA)\n\
10-) PHP 7.1 (Ondrej PPA)\n11-) PHP 7.0 (Ondrej PPA)\n12-) PHP 5.6 (Ondrej PPA)\n\nPlease Select Your PHP Version(Please Input Between 1-12):"
            while true;do
                read -r php_version
                if [[ $php_version = "1" ]] || [[ $php_version = "2" ]] || [[ $php_version = "3" ]] || [[ $php_version = "4" ]] || [[ $php_version = "5" ]] \
                || [[ $php_version = "6" ]] || [[ $php_version = "7" ]] || [[ $php_version = "8" ]] || [[ $php_version = "9" ]] || [[ $php_version = "10" ]] \
                || [[ $php_version = "11" ]] || [[ $php_version = "12" ]];then
                    break
                else
                    printf "Please Input Between 1-12:"
                fi
            done
        elif [ "$opt" = 2 ];then
            printf "\nPlease Choose Your Desired Nginx Version\n\n1-) Nginx (Official Package)\n\
2-) Nginx Latest(Mainline Compile From Source)\n3-) Nginx Stable(Compile From Source)\n\
4-) Nginx (From nginx.list Stable)\n5-) Nginx (From nginx.list Mainline)\n\nPlease Select Your Nginx Version(Please Input Between 1-5):" 
            while true;do
                read -r nginxversion
                if [[ $nginxversion = "1" ]] || [[ $nginxversion = "2" ]] || [[ $nginxversion = "3" ]] || [[ $nginxversion = "4" ]] || [[ $nginxversion = "5" ]];then
                    printf "Chosen option $nginxversion\n"
                    break
                else
                    printf "Please Input Between 1-5:"
                fi
            done
        elif [ "$opt" = 3 ];then
            printf "\nPlease Choose Your Desired Grub Customizer Version\n\n1-) Grub Customizer (From PPA)\n\
2-) Grub Customizer (Mainline Compile From Source)\n\nPlease Select Your Grub Customizer Version(Please Input Between 1-2):"
            while true;do
                read -r grub_customizer_version
                if [[ $grub_customizer_version = "1" ]] || [[ $grub_customizer_version = "2" ]];then
                    printf "Chosen option $grub_customizer_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 4 ];then
            printf "\nPlease Choose Your Desired Apache Version\n\n1-) Apache (From Official Package)\n\
2-) Apache Latest (From Ondrej PPA)\n\nPlease Select Apache Version(Please Input Between 1-2):"
            while true;do
                read -r apache_version
                if [[ $apache_version = "1" ]] || [[ $apache_version = "2" ]];then
                    printf "Chosen option $apache_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 5 ];then
            printf "\nPlease Choose Your Desired FFmpeg Version\n\n1-) FFmpeg (From Official Package)\n\
2-) FFmpeg (From savoury1 PPA)\n3-) FFmpeg (From Snap)\n\nPlease Select FFmpeg Version(Please Input Between 1-3):"
            while true;do
                read -r ffmpeg_version
                if [[ $ffmpeg_version = "1" ]] || [[ $ffmpeg_version = "2" ]] || [[ $ffmpeg_version = "3" ]];then
                    printf "Chosen option $ffmpeg_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 6 ];then
            printf "\nPlease Choose Your Desired WineHQ Version\n\n1-) WineHQ Stable (From Official Package)\n\
2-) WineHQ Development (From Official Package)\n3-) WineHQ Stable (With Wine Official Repository)\n\
4-) WineHQ Development (With Wine Official Repository)\n5-) WineHQ Staging (With Wine Official Repository)\n\
\n\nPlease Select WineHQ Version(Please Input Between 1-5):"
            while true;do
                read -r winehq_version
                if [[ $winehq_version = "1" ]] || [[ $winehq_version = "2" ]] || [[ $winehq_version = "3" ]] || [[ $winehq_version = "4" ]] || [[ $winehq_version = "5" ]];then
                    printf "Chosen option $winehq_version\n"
                    break
                else
                    printf "Please Input Between 1-5:"
                fi
            done
        elif [ "$opt" = 7 ];then
            printf "\nPlease Choose Your Desired Nmap Version\n\n1-) Nmap (Official Package)\n\
2-) Nmap Latest (From .rpm file to .deb file)\n3-) Nmap Latest (Compile From Source Code)\n\
4-) Nmap Latest (Via Snap)\n\nPlease Select Nmap Version(Please Input Between 1-4):"
            while true;do
                read -r nmap_version
                if [[ $nmap_version = "1" ]] || [[ $nmap_version = "2" ]] || [[ $nmap_version = "3" ]] || [[ $nmap_version = "4" ]];then
                    printf "Chosen option $nmap_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 8 ];then
            printf "\nPlease Choose Your Desired OpenSSH Version\n\n1-) OpenSSH (Official Package)\n\
2-) OpenSSH Latest (Compile From Source)\n\nPlease Select Your OpenSSH Version(Please Input Between 1-2):"
            while true;do
                read -r openssh_version
                if [[ $openssh_version = "1" ]] || [[ $openssh_version = "2" ]];then
                    printf "Chosen option $openssh_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        elif [ "$opt" = 9 ];then
            printf "\nPlease Choose Your Desired OpenSSL Version\n\n1-) OpenSSL (From Official Package)\n\
2-) OpenSSL 1.1 Latest (Compile From Source)\n3-) OpenSSL 3.0 Latest (Compile From Source)\n\
\n\nPlease Select Your OpenSSL Version(Please Input Between 1-3):"
            while true;do
                read -r openssl_version
                if [[ $openssl_version = "1" ]] || [[ $openssl_version = "2" ]] || [[ $openssl_version = "3" ]];then
                    printf "Chosen option $openssl_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 10 ];then
            printf "\nPlease Choose Your Desired Redis Version\n\n1-) Redis (Official Package)\n\
2-) Redis Latest (With Snap)\n3-) Redis Latest (Compile From Source)\n\nPlease Select Your Redis Version(Please Input Between 1-3):"
            while true;do
                read -r redis_version
                if [[ $redis_version = "1" ]] || [[ $redis_version = "2" ]] || [[ $redis_version = "3" ]];then
                    printf "Chosen option $redis_version\n"
                    break
                else
                    printf "Please Input Between 1-3:"
                fi
            done
        elif [ "$opt" = 11 ];then
            printf "\nPlease Choose Your Desired Jenkins Version\n\n1-) Jenkins (LTS)\n\
2-) Jenkins (Latest)\n3-) Jenkins (Docker LTS)\n4-) Jenkins (Docker Latest)\n\nPlease Select Your Jenkins Version(Please Input Between 1-4):"
            while true;do
                read -r jenkins_version
                if [[ $jenkins_version = "1" ]] || [[ $jenkins_version = "2" ]] || [[ $jenkins_version = "3" ]] || [[ $jenkins_version = "4" ]];then
                    printf "Chosen option $jenkins_version\n"
                    break
                else
                    printf "Please Input Between 1-4:"
                fi
            done
        elif [ "$opt" = 12 ];then
            printf "\nPlease Choose Your Desired Docker Version\n\n1-) Docker (LTS)\n\
2-) Docker (Stable .deb)\n\n Please Select Your Docker Version(Please Input Between 1-2):"
            while true;do
                read -r docker_version
                if [[ $docker_version = "1" ]] || [[ $docker_version = "2" ]];then
                    printf "Chosen option $docker_version\n"
                    break
                else
                    printf "Please Input Between 1-2:"
                fi
            done
        else
            :
        fi
    fi
done

if [ "${opts[opt]}" = "" ];then
    exit
fi

# Loading Bar

printf "Installation starting"
value=0
while [ $value -lt 600 ];do
    value=$((value+20))
    printf "."
    sleep 0.05
done
printf "\n"

#Install Necessary Packages

sudo apt update
sudo apt -y install bash-completion net-tools wget curl lynx
source /etc/profile.d/bash_completion.sh
grep -qxF 'source /etc/profile.d/bash_completion.sh' /root/.bashrc || echo 'source /etc/profile.d/bash_completion.sh' >> /root/.bashrc

# Create Downloads folder if doesn't exist
if [ -d "/root/Downloads/" ];then
    :
else
    sudo mkdir -pv /root/Downloads/
fi

# INSTALLATION BY SELECTION
for opt in "${!opts[@]}";do
    if [[ ${opts[opt]} ]];then
        case $opt in
            1) 
            #PHP
            . "$scripts_path/1-Php.sh"
            printf "\nPhp Installation Has Finished\n\n"
            ;;
            2)
            # 2- Nginx
            . "$scripts_path/2-Nginx.sh"
            printf "\nNginx Installation Has Finished\n\n"
            ;;
            3)
            # 3- Grub Customizer
            . "$scripts_path/3-Grub-Customizer.sh"
            printf "\nGrub Customizer Installation Has Finished\n\n"
            ;;
            4)
            # 4- Apache
            . "$scripts_path/4-Apache.sh"
            printf "\nApache Installation Has Finished\n\n"
            ;;
            5)
            # 5- FFmpeg
            . "$scripts_path/5-FFmpeg.sh"
            printf "\nFFmpeg Installation Has Finished\n\n"
            ;;
            6)
            # 6- WineHQ
            . "$scripts_path/6-WineHQ.sh"
            printf "\nWineHQ Installation Has Finished\n\n"
            ;;
            7)
            # 7- Nmap
            . "$scripts_path/7-Nmap.sh"
            printf "\nNmap Installation Has Finished\n\n"
            ;;
            8)
            # 8- OpenSSH
            . "$scripts_path/8-OpenSSH.sh"
            printf "\nOpenSSH Installation Has Finished\n\n"
            ;;
            9)
            # 9- OpenSSL
            . "$scripts_path/9-OpenSSL.sh"
            printf "\nOpenSSL Installation Has Finished\n\n"
            ;;
            10)
            # 10- Redis
            . "$scripts_path/10-Redis.sh"
            printf "\nRedis Installation Has Finished\n\n"
            ;;
            11)
            # 11- Jenkins
            . "$scripts_path/11-Redis.sh"
            printf "\nRedis Installation Has Finished\n\n"
            ;;
            12)
            # 12- Docker
            . "$scripts_path/12-Docker.sh"
            printf "\nDocker Installation Has Finished\n\n"
            ;;
        esac
    fi
done