#!/bin/bash

#1-Php installer.sh
printf "\nPlease Choose Your Desired PHP Version \n\n1-) PHP 8.1 (Ubuntu Official Package)\n\
2-) PHP 8.0 (Ondrej PPA)\n3-) PHP 7.4 (Ondrej PPA)\n4-) PHP 7.3 (Ondrej PPA)\n\
5-) PHP 7.2 (Ondrej PPA)\n6-) PHP 7.1 (Ondrej PPA)\n7-) PHP 7.0 (Ondrej PPA)\n\
8-) PHP 5.6 (Ondrej PPA)\n\nPlease Select Your PHP Version:"
read -r php_version
if [ "$php_version" = "1" ];then
    sudo apt -y install php8.1 php8.1-dev php8.1-mysql php8.1-bcmath php8.1-bz2 php8.1-cgi php8.1-cli \
    php8.1-common php8.1-curl php8.1-fpm php8.1-gnupg php8.1-imagick php8.1-memcache php8.1-memcached \
    php8.1-mongodb php8.1-odbc php8.1-opcache php8.1-pgsql php8.1-redis php8.1-soap php8.1-sqlite3 \
    php8.1-xml php8.1-yac php8.1-yaml php8.1-zip
    systemctl restart php8.1-fpm.service
    systemctl enable php8.1-fpm.service
elif [ "$php_version" = "2" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php8.0 php8.0-dev php8.0-mysql php8.0-bcmath php8.0-bz2 php8.0-cgi php8.0-cli \
    php8.0-common php8.0-curl php8.0-fpm php8.0-gnupg php8.0-imagick php8.0-memcache php8.0-memcached \
    php8.0-mongodb php8.0-odbc php8.0-opcache php8.0-pgsql php8.0-redis php8.0-soap php8.0-sqlite3 \
    php8.0-xml php8.0-yac php8.0-yaml php8.0-zip
    systemctl restart php8.0-fpm.service
    systemctl enable php8.0-fpm.service
elif [ "$php_version" = "3" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.4 php7.4-dev php7.4-mysql php7.4-bcmath php7.4-bz2 php7.4-cgi php7.4-cli \
    php7.4-common php7.4-curl php7.4-fpm php7.4-gnupg php7.4-imagick php7.4-memcache php7.4-memcached \
    php7.4-mongodb php7.4-odbc php7.4-opcache php7.4-pgsql php7.4-redis php7.4-soap php7.4-sqlite3 \
    php7.4-xml php7.4-yac php7.4-yaml php7.4-zip
    systemctl restart php7.4-fpm.service
    systemctl enable php7.4-fpm.service
elif [ "$php_version" = "4" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.3 php7.3-dev php7.3-mysql php7.3-bcmath php7.3-bz2 php7.3-cgi php7.3-cli \
    php7.3-common php7.3-curl php7.3-fpm php7.3-gnupg php7.3-imagick php7.3-memcache php7.3-memcached \
    php7.3-mongodb php7.3-odbc php7.3-opcache php7.3-pgsql php7.3-redis php7.3-soap php7.3-sqlite3 \
    php7.3-xml php7.3-yac php7.3-yaml php7.3-zip
    systemctl restart php7.3-fpm.service
    systemctl enable php7.3-fpm.service
elif [ "$php_version" = "5" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.2 php7.2-dev php7.2-mysql php7.2-bcmath php7.2-bz2 php7.2-cgi php7.2-cli \
    php7.2-common php7.2-curl php7.2-fpm php7.2-gnupg php7.2-imagick php7.2-memcache php7.2-memcached \
    php7.2-mongodb php7.2-odbc php7.2-opcache php7.2-pgsql php7.2-redis php7.2-soap php7.2-sqlite3 \
    php7.2-xml php7.2-yac php7.2-yaml php7.2-zip
    systemctl restart php7.2-fpm.service
    systemctl enable php7.2-fpm.service
elif [ "$php_version" = "6" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.1 php7.1-dev php7.1-mysql php7.1-bcmath php7.1-bz2 php7.1-cgi php7.1-cli \
    php7.1-common php7.1-curl php7.1-fpm php7.1-gnupg php7.1-imagick php7.1-memcache php7.1-memcached \
    php7.1-mongodb php7.1-odbc php7.1-opcache php7.1-pgsql php7.1-redis php7.1-soap php7.1-sqlite3 \
    php7.1-xml php7.1-yac php7.1-yaml php7.1-zip
    systemctl restart php7.1-fpm.service
    systemctl enable php7.1-fpm.service
elif [ "$php_version" = "7" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.0 php7.0-dev php7.0-mysql php7.0-bcmath php7.0-bz2 php7.0-cgi php7.0-cli \
    php7.0-common php7.0-curl php7.0-fpm php7.0-gnupg php7.0-imagick php7.0-memcache php7.0-memcached \
    php7.0-mongodb php7.0-odbc php7.0-opcache php7.0-pgsql php7.0-redis php7.0-soap php7.0-sqlite3 \
    php7.0-xml php7.0-yac php7.0-yaml php7.0-zip
    systemctl restart php7.0-fpm.service
    systemctl enable php7.0-fpm.service
elif [ "$php_version" = "8" ];then
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php5.6 php5.6-dev php5.6-mysql php5.6-bcmath php5.6-bz2 php5.6-cgi php5.6-cli \
    php5.6-common php5.6-curl php5.6-fpm php5.6-gnupg php5.6-imagick php5.6-memcache php5.6-memcached \
    php5.6-mongodb php5.6-odbc php5.6-opcache php5.6-pgsql php5.6-redis php5.6-soap php5.6-sqlite3 \
    php5.6-xml php5.6-yac php5.6-yaml php5.6-zip
    systemctl restart php5.6-fpm.service
    systemctl enable php5.6-fpm.service
else
    echo "Out of options please choose between 1-8"
fi