#!/bin/bash

#6-WineHQ installer.sh
printf "\nPlease Choose Your Desired WineHQ Version\n\n1-) WineHQ Stable (From Official Package)\n\
2-) WineHQ Development (From Official Package)\n3-) WineHQ Stable (With Wine Official Repository)\n\
4-) WineHQ Development (With Wine Official Repository)\n5-) WineHQ Staging (With Wine Official Repository)\n\
\n\nPlease Select WineHQ Version:"
read -r winehq_version
if [ "$winehq_version" = "1" ];then
    sudo apt -y install winehq-stable
elif [ "$winehq_version" = "2" ];then
    sudo apt -y install winehq-development
elif [ "$winehq_version" = "3" ];then
    if [ "$cpuarch" = "x86_64" ];then
        sudo dpkg --add-architecture i386
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-stable
    else
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-stable
    fi
elif [ "$winehq_version" = "4" ];then
    if [ "$cpuarch" = "x86_64" ];then
        sudo dpkg --add-architecture i386
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-devel
    else
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-devel
    fi
elif [ "$winehq_version" = "5" ];then
    if [ "$cpuarch" = "x86_64" ];then
        sudo dpkg --add-architecture i386
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-staging
    else
        wget -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
        wget -O /etc/apt/sources.list.d/winehq-jammy.sources https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
        sudo apt update
        sudo apt -y install --install-recommends winehq-staging
    fi
else
    echo "Out of options please choose between 1-5"
fi