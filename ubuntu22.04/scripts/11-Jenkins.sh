#!/bin/bash

#11-Jenkins installer

printf "\nPlease Choose Your Desired Jenkins Version\n\n1-) Jenkins (LTS)\n\
2-) Jenkins (Latest)\n3-) Jenkins (Docker LTS)\n4-) Jenkins (Docker Latest)\nPlease Select Your Jenkins Version:"
read -r jenkins_version
if [ "$jenkins_version" = "1" ];then
    curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
    /usr/share/keyrings/jenkins-keyring.asc > /dev/null
    echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
     sudo apt-get update
    sudo apt-get -y install fontconfig openjdk-11-jre
    sudo apt-get -y install jenkins
elif [ "$jenkins_version" = "2" ];then
    curl -fsSL https://pkg.jenkins.io/debian/jenkins.io.key | sudo tee \
    /usr/share/keyrings/jenkins-keyring.asc > /dev/null
    echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
    sudo apt-get update
    sudo apt-get -y install fontconfig openjdk-11-jre
    sudo apt-get -y install jenkins
elif [ "$jenkins_version" = "3" ];then
    #Docker Installation Parts
    printf "\nPlease Choose Your Desired Docker Version\n\n1-) Docker (LTS)\n\
2-) Docker (Stable .deb)\n\n Please Select Your Docker Version:"
read -r docker_version
if [ "$docker_version" = "1" ];then
    sudo apt-get -y remove docker docker-engine docker.io containerd runc
    sudo apt-get update
    sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
    docker pull jenkins/jenkins:lts-jdk11
elif [ "$docker_version" = "2" ];then
    if [ "$cpuarch" = "x86_64" ];then
        containerd_stable_deb=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/jammy/pool/stable/amd64/ \
        | awk '/http/{print $2}' | grep -i containerd.io | tail -n 1)
        wget -O /root/Downloads/latest-containerd.deb "$containerd_stable_deb"
        sudo dpkg -i "$containerd_stable_deb"
        docker_ce_cli=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/jammy/pool/stable/amd64/ \
        | awk '/http/{print $2}' | grep -iv "docker-ce-rootless" | grep -i "docker-ce-cli" | tail -n 1)
        wget -O /root/Downloads/latest_docker-ce-cli.deb "$docker_ce_cli"
        sudo dpkg -i "$docker_ce_cli"
        latest_docker_deb=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/jammy/pool/stable/amd64/ | awk '/http/{print $2}' \
        | grep -iv "docker-ce-cli\|docker-ce-rootless" | grep -i docker-ce | head -n 1)
        wget -O /root/Downloads/latest-docker-ce.deb "$latest_docker_deb"
        sudo dpkg -i "$latest_docker_deb"
        docker pull jenkins/jenkins:lts-jdk11
    else
        echo "Your cpu is different than x86_64"
    fi
else
    echo "Out of options please choose between 1-2"
fi
elif [ "$jenkins_version" = "4" ];then
    #Docker Installation Section
    printf "\nPlease Choose Your Desired Docker Version\n\n1-) Docker (LTS)\n\
2-) Docker (Stable .deb)\n\n Please Select Your Docker Version:"
read -r docker_version
if [ "$docker_version" = "1" ];then
    sudo apt-get -y remove docker docker-engine docker.io containerd runc
    sudo apt-get update
    sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
    docker pull jenkins/jenkins:jdk11
elif [ "$docker_version" = "2" ];then
    if [ "$cpuarch" = "x86_64" ];then
        containerd_stable_deb=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/jammy/pool/stable/amd64/ \
        | awk '/http/{print $2}' | grep -i containerd.io | tail -n 1)
        wget -O /root/Downloads/latest-containerd.deb "$containerd_stable_deb"
        sudo dpkg -i "$containerd_stable_deb"
        docker_ce_cli=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/jammy/pool/stable/amd64/ \
        | awk '/http/{print $2}' | grep -iv "docker-ce-rootless" | grep -i "docker-ce-cli" | tail -n 1)
        wget -O /root/Downloads/latest_docker-ce-cli.deb "$docker_ce_cli"
        sudo dpkg -i "$docker_ce_cli"
        latest_docker_deb=$(lynx -dump https://download.docker.com/linux/ubuntu/dists/jammy/pool/stable/amd64/ | awk '/http/{print $2}' \
        | grep -iv "docker-ce-cli\|docker-ce-rootless" | grep -i docker-ce | head -n 1)
        wget -O /root/Downloads/latest-docker-ce.deb "$latest_docker_deb"
        sudo dpkg -i "$latest_docker_deb"
        docker pull jenkins/jenkins:jdk11
    else
        echo "Your cpu is different than x86_64"
    fi
else
    echo "Out of options please choose between 1-2"
fi
    docker pull jenkins/jenkins:jdk11
else
    echo "Out of options please choose between 1-4"
fi